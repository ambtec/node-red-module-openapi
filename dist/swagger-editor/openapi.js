"use strict";

class OpenApi {
  constructor(jQuery, selectId) {
    this.node = void 0;
    this.swaggerEditorDOM = void 0;
    this.jquery = void 0;
    this.selectId = void 0;
    this.currentConfiguration = "";
    this.configurationEndpoint = "/admin/openapi/configuration";
    this.swaggerYamlEndpoint = "/{project}/swagger.yaml";
    this.mainProjectKey = "";
    this.swaggerEditorEndpoint = "/{project}/swagger-editor/index.html";
    this.socketEndpoint = "";
    this.externalSources = ["https://cdnjs.cloudflare.com/ajax/libs/yamljs/0.3.0/yaml.min.js"];
    this.maxUpdateTries = 100;
    this.maxUpdateTimeoutInMs = 100;
    this.updateInterval = null;
    this.observer = new Map();
    this.jquery = jQuery;
    this.selectId = selectId;
    this.setConfiguration();

    for (const source of this.externalSources) {
      this.jquery("<script>", {
        src: source
      }).appendTo("head");
    }
  }

  async setConfiguration() {
    const configuration = await this.getConfiguration().catch(e => console.error);
    this.swaggerEditorEndpoint = configuration.ORCHESTRATOR_ENDPOINT + this.swaggerEditorEndpoint;
    this.swaggerYamlEndpoint = configuration.ORCHESTRATOR_ENDPOINT + this.swaggerYamlEndpoint;
    this.mainProjectKey = configuration.MAIN_PROJECT;
    this.socketEndpoint = configuration.SOCKETIO_ENDPOINT;
  }

  getConfiguration() {
    return new Promise(async (resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open("GET", this.configurationEndpoint);
      xhr.setRequestHeader("Content-Type", "application/json");

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (200 === xhr.status) {
            resolve(JSON.parse(xhr.responseText));
          } else {
            reject(xhr);
          }
        }
      };

      xhr.ontimeout = function (e) {
        console.error("Could not fetch configuration", "Timeout", e);
      };

      xhr.onerror = () => {
        reject(xhr);
      };

      xhr.send();
    });
  }

  setStorageContent(id, content) {
    window.localStorage.setItem(id, content);
  }

  getStorageContent(id) {
    return window.localStorage.getItem(id);
  }

  setEditorContent(content) {
    this.setStorageContent("swagger-editor-content", content);
  }

  getEditorContent() {
    return this.getStorageContent("swagger-editor-content");
  }

  updateParentSettings(data, i = 0) {
    if (i > this.maxUpdateTries) {
      console.error("Could not update parent form field");
      return;
    }

    if ("" === data.option) return;
    const inputUrl = this.jquery("#node-input-url");
    const inputSocketId = this.jquery("#node-input-socketid");

    if (inputUrl.length) {
      const inputMethod = this.jquery("#node-input-method");
      const inputUpload = this.jquery("#node-input-upload");
      const options = this.parseSelectValue(data.option);
      const json = this.getJsonConfiguration();
      const config = json.paths[options.path] ? json.paths[options.path][options.method] : null;
      inputUpload.prop("checked", false);

      if (config && config.parameters) {
        for (const parameter of config.parameters) {
          if ("formData" === parameter.in) {
            inputUpload.prop("checked", true);
          }
        }
      }

      inputUrl.val(options.path);
      inputMethod.val(options.method).change();
    } else if (inputSocketId.length) {
      const options = this.parseSelectValue(data.option);
      inputSocketId.val(options.topic);
    } else {
      // wait till form is ready
      window.requestAnimationFrame(this.updateParentSettings.bind(this, data, i + 1));
    }
  }

  saveNewConfiguration() {
    const currentVal = this.node.path ? this.node.path : "";
    const project = this.observer.get("currentProject");
    const url = new URL(this.swaggerYamlEndpoint.replace("{project}", project));
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (200 !== xhr.status) {
          console.error("Configuration could not be saved", xhr.status, xhr.responseText);
        }
      }
    };

    xhr.ontimeout = function (e) {
      console.error("Configuration could not be saved", "Timeout", e);
    };

    const data = {
      nodeId: this.node.id,
      option: currentVal,
      config: this.getEditorContent()
    };
    xhr.send(JSON.stringify(data));
    this.updateParentSettings(data);
  }

  parseSelectValue(value) {
    const regexPath = new RegExp("(^\\/[^\\s\\[\\(]+)", "g");
    const path = regexPath.exec(value);
    const regexMethod = new RegExp("\\(([^\\)]+)\\)", "g");
    const method = regexMethod.exec(value);
    const regexTopic = new RegExp("\\<([^\\]]+)\\>", "g");
    const topic = regexTopic.exec(value);
    const parsedTopic = topic && topic[1] ? topic[1].split(":") : null;
    const result = {
      path: path && path[1] ? path[1] : null,
      method: method && method[1] ? method[1] : null,
      topic: parsedTopic && parsedTopic[1] ? parsedTopic[1] : topic && topic[1] ? topic[1] : null
    };
    return result;
  }

  getSelectValue(path, method = "") {
    const socketPathRegex = new RegExp(`^(${this.socketEndpoint})\\s*\\<(.+)\\>`);
    const socketConfig = socketPathRegex.exec(path);

    if (socketConfig) {
      const [type, topic] = socketConfig[2].split(":");
      return type && topic && type === method ? `${socketConfig[1]} <${socketConfig[2]}>` : null;
    }

    if ("" !== method) {
      return `${path} (${method})`;
    }

    return `${path}`;
  }

  getJsonConfiguration() {
    try {
      // @ts-ignore
      return YAML.parse(this.currentConfiguration);
    } catch (e) {
      return null;
    }
  }

  sortOptionsByValue(a, b) {
    return this.jquery(b).val() < this.jquery(a).val();
  }

  async updateRouteSelectOptions() {
    // console.log('node', this.node)
    const parentNodeBySwaggerId = this.observer.get("parentNode");
    const parentNodeBySwaggerIdType = parentNodeBySwaggerId ? parentNodeBySwaggerId : "any";
    const json = this.getJsonConfiguration();
    const select = this.jquery(this.selectId);
    const currentVal = this.node.path ? this.node.path : "";
    select.empty();
    select.append(this.jquery("<option></option>").attr("value", "").text(this.node._("swagger.config.select.option.default.label")));
    const optgroupHttp = this.jquery("<optgroup></optgroup>").attr("label", this.node._("swagger.config.select.optgroup.http.label"));
    const optgroupSocketIn = this.jquery("<optgroup></optgroup>").attr("label", this.node._("swagger.config.select.optgroup.socket.label"));
    const optgroupSocketOut = this.jquery("<optgroup></optgroup>").attr("label", this.node._("swagger.config.select.optgroup.socket.label")); // Collect options

    if (json && json.paths) {
      for (const path in json.paths) {
        for (const method in json.paths[path]) {
          if (path.indexOf(this.socketEndpoint) > -1) {
            let value;

            switch (parentNodeBySwaggerIdType) {
              case "websocket-in":
              case "socketio-receive":
                value = this.getSelectValue(path, "in");
                if (value) optgroupSocketIn.append(this.jquery("<option></option>").attr("value", value).text(value));
                break;

              case "websocket-out":
              case "socketio-reply":
                value = this.getSelectValue(path, "out");
                if (value) optgroupSocketOut.append(this.jquery("<option></option>").attr("value", value).text(value));
                break;
            }
          } else {
            const value = this.getSelectValue(path, method);
            optgroupHttp.append(this.jquery("<option></option>").attr("value", value).text(value));
          }
        }
      }

      optgroupSocketIn.children().detach().sort(this.sortOptionsByValue.bind(this)).appendTo(optgroupSocketIn);
      optgroupSocketOut.children().detach().sort(this.sortOptionsByValue.bind(this)).appendTo(optgroupSocketOut);
      optgroupHttp.children().detach().sort(this.sortOptionsByValue.bind(this)).appendTo(optgroupHttp);
    } // Assign options


    switch (parentNodeBySwaggerIdType) {
      case "http in":
        if (optgroupHttp.find("option").length) select.append(optgroupHttp);
        break;

      case "websocket-in":
      case "socketio-receive":
        if (optgroupSocketIn.find("option").length) select.append(optgroupSocketIn);
        break;

      case "websocket-out":
      case "socketio-reply":
        if (optgroupSocketOut.find("option").length) select.append(optgroupSocketOut);
        break;

      default:
        if (optgroupHttp.find("option").length) select.append(optgroupHttp);
        if (optgroupSocketIn.find("option").length) select.append(optgroupSocketIn);
        if (optgroupSocketOut.find("option").length) select.append(optgroupSocketOut);
    }

    select.val(currentVal);
  }

  checkForSwaggerEditorChanges() {
    const config = this.getEditorContent();

    if (config && this.currentConfiguration !== config) {
      this.currentConfiguration = config;
      this.updateRouteSelectOptions().then().catch();
    }
  }

  setNode(node) {
    this.node = node;
  }

  save() {
    // Set timeout to make sure swagger-editor update object first, then save config
    setTimeout(this.saveNewConfiguration.bind(this), 100);
  }

  handleMainProjectInputs() {
    const currentProject = this.observer.get("currentProject"); // Only continue with main project

    if (currentProject !== this.mainProjectKey) {
      return;
    }

    const currentNode = this.observer.get("currentNode");

    switch (currentNode) {
      case "http in":
        this.jquery("#node-input-url").prop("disabled", true);
        this.jquery("#node-input-method").prop("disabled", true);
        this.jquery("#node-input-upload").prop("disabled", true);
        break;

      case "websocket-in":
      case "socketio-receive":
        this.jquery("#node-input-socketid").prop("disabled", true);
        break;

      case "websocket-out":
      case "socketio-reply":
        this.jquery("#node-input-sockettype").prop("disabled", true);
        this.jquery("#node-input-socketid").prop("disabled", true);
        this.jquery("#node-input-payloadkey").prop("disabled", true);
        this.jquery("#node-input-room").prop("disabled", true);
        break;

      case "swagger-doc":
        this.jquery("#node-config-input-path").prop("disabled", true);
        break;
    }
  }

  updateObserver() {
    const currentNodeInfos = this.jquery("#red-ui-sidebar-content .red-ui-info-table .red-ui-help-info-row");

    if (currentNodeInfos.length) {
      for (const currentNodeInfo of currentNodeInfos) {
        if ("type" === this.jquery(currentNodeInfo).find("td:nth-of-type(1)").html().toLowerCase()) {
          const currentNode = this.jquery(currentNodeInfo).find("td:nth-of-type(2)").html();
          const observerCurrentNode = this.observer.get("currentNode");

          if (currentNode !== observerCurrentNode) {
            this.observer.set("currentNode", currentNode);
            this.observer.set("parentNode", observerCurrentNode);
          }

          break;
        }
      }
    }

    const currentProject = this.jquery("#red-ui-sidebar-content .red-ui-info-outline-project .red-ui-info-outline-item-label");

    if (currentProject.length) {
      this.observer.set("currentProject", currentProject.html());
    }
  }

  onAnimationFrame() {
    this.updateObserver();
    this.handleMainProjectInputs();
    const currentNode = this.observer.get("currentNode");

    if ("swagger-doc" === currentNode) {
      this.checkForSwaggerEditorChanges();
    }

    window.requestAnimationFrame(this.onAnimationFrame.bind(this));
  }

  initObserver() {
    this.observer.set("currentNode", null);
    this.observer.set("parentNode", null);
    this.observer.set("currentProject", null);
  }

  initSwaggerEditor() {
    this.currentConfiguration = "";
    const project = this.observer.get("currentProject");
    const url = this.swaggerEditorEndpoint.replace("{project}", project);
    this.swaggerEditorDOM = this.jquery("<iframe/>", {
      src: `${url}`,
      style: "width:100%; height:calc(100vh - 320px)"
    });
    this.jquery("#swagger-tab-editor").append(this.swaggerEditorDOM);
  }

  init() {
    this.setEditorContent("");
    this.initObserver();
    window.requestAnimationFrame(this.onAnimationFrame.bind(this));
  }

}